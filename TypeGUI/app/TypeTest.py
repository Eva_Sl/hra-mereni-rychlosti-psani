import tkinter as tk
from tkinter import messagebox
import random
import time

class TypingTestApp:
    def __init__(self, master):
        # Initialize the Typing Test application
        self.master = master
        self.master.title("Typing Test")  # Set the title of the window
        self.master.configure(bg='black')  # Set the background color of the window
        self.master.resizable(width=False, height=False)  # Making window not resizable

        # Variable to store highscore
        self.highscore = tk.StringVar()
        # Load highscore from file
        self.load_highscore()

        # Label to display the sentence to be typed
        self.sentence_label = tk.Label(master, text="", font=("Arial", 14), bg='black', fg='white')
        self.sentence_label.pack(pady=(20, 5), padx=10)

        # Entry box for user input
        self.user_input = tk.Entry(master, font=("Arial", 12), width=50)
        self.user_input.pack(pady=5, padx=(10, 10))

        # Label to display timer
        self.timer_label = tk.Label(master, text="", font=("Arial", 12), bg='black', fg='white')
        self.timer_label.pack()

        # Label to display words per minute (WPM)
        self.wpm_label = tk.Label(master, text="", font=("Arial", 12), bg='black', fg='white')
        self.wpm_label.pack()

        # Label to display highscore
        self.highscore_label = tk.Label(master, textvariable=self.highscore, font=("Arial", 12), bg='black', fg='white')
        self.highscore_label.pack()

        # Button to reset the test
        self.reset_button = tk.Button(master, text="Reset", command=self.reset, bg='gray', fg='white', font=("Arial", 12))
        self.reset_button.pack(pady=5, padx=(10, 10))

        # Button to reset the highscore
        self.reset_highscore_button = tk.Button(master, text="Reset Highscore", command=self.reset_highscore, bg='gray', fg='white', font=("Arial", 12))
        self.reset_highscore_button.pack(pady=5, padx=(10, 10))

        # Label to display prompt for incorrect input
        self.prompt_label = tk.Label(master, text="", font=("Arial", 12), bg='black', fg='red')
        self.prompt_label.pack(pady=(5, 20), padx=10)

        # Load a random sentence for typing
        self.load_sentence()
        self.start_time = None
        self.measurement_done = False

    # Load a random sentence from a file
    def load_sentence(self):
        with open("sentences.txt", "r") as file:
            sentences = file.readlines()
        self.sentence = random.choice(sentences).strip()
        self.sentence_label.config(text=self.sentence)

    # Method to check user typing
    def check_typing(self, event):
        if self.measurement_done:
            # Reset if the measurement is already done
            self.user_input.delete(0, 'end')  # Clear the entry box
            self.user_input.config(state='normal')  # Enable the entry box
            return

        user_text = self.user_input.get()
        for i in range(len(user_text)):
            if i < len(self.sentence):
                if user_text[i] != self.sentence[i]:
                    self.prompt_label.config(text="Incorrect input")  # Show prompt for incorrect input
                    return  # Exit the function if there's a mismatch
        self.prompt_label.config(text="")  # Hide prompt if input is correct

        if user_text == self.sentence:
            # Calculate WPM if the sentence is correctly typed
            end_time = time.time()
            elapsed_time = end_time - self.start_time
            words_typed = len(self.sentence.split())
            wpm = int(words_typed / (elapsed_time / 60))
            self.wpm_label.config(text=f"WPM: {wpm}")

            # Update highscore if necessary
            if wpm > int(self.highscore.get() or 0):
                self.highscore.set(wpm)
                self.save_highscore()

            self.measurement_done = True
            self.user_input.delete(0, 'end')  # Clear the entry box
            self.user_input.config(state='disabled')  # Disable the entry box
            self.timer_label.config(text="Press 'Reset' for new attempt")

        elif self.start_time is None:
            # Start timer when user starts typing
            self.start_time = time.time()

    # Reset the typing test
    def reset(self):
        self.user_input.delete(0, 'end')
        self.start_time = None
        self.measurement_done = False
        self.timer_label.config(text="")
        self.wpm_label.config(text="")
        self.user_input.config(state='normal')
        self.prompt_label.config(text="")  # Hide prompt when reset
        self.load_sentence()

    # Load the highscore from file
    def load_highscore(self):
        try:
            with open("highscore.txt", "r") as file:
                highscore = file.readline()
                self.highscore.set(highscore)
        except FileNotFoundError:
            pass

    # Save the highscore to file
    def save_highscore(self):
        with open("highscore.txt", "w") as file:
            file.write(self.highscore.get())

    # Reset the highscore
    def reset_highscore(self):
        if messagebox.askyesno("Reset Highscore", "Are you sure you want to reset your highscore?"):
            self.highscore.set("0")
            self.save_highscore()

def main():
    root = tk.Tk()
    app = TypingTestApp(root)
    root.bind('<Key>', app.check_typing)  # Binding key events to the check_typing method
    root.mainloop()

if __name__ == "__main__":
    main()


